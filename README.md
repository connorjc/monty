# monty

A python based tool to help software developers learn about their own work habits with intuitive data graphs. The idea is to use Monty to recognize how and when you are distracted or most productive.

## DEPENDENCIES

* liblz4-dev
* python3-dev
* Upstart (found in Ubuntu16.04 or less)
* Firefox

## SETUP:
```sh
git clone https://connorjc@bitbucket.org/connorjc/monty.git
cd monty
python3 -m venv env
cd env
source bin/activate
git clone https://github.com/andikleen/lz4json.git
cd lz4json
make
mv lz4jsoncat ../bin/.
cd ../../
pip install -q -r requirements.txt
```

## INSTALL:
```sh
./install.sh
```

## Access Webpage:
Visit localhost:5000 in the browser.
