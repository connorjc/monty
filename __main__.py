"""Main driver for backend of the monty web application.

This file is the main logic for the backend of the application. It is run
independently from the frontend's scripts. What this does is monitor what
programs are currently running and what pages are visited in Firefox and
logs it to a sqlite database.

As this is a non-invasive program, and the data gathered may be deemed
personal, all the data is saved in the sqlite database that you can remove
at anytime. No duplicates are ever created.

Author: Connor Christian
"""

from multiprocessing import Process, Manager, Queue, Event
from enum import IntEnum
import re
import os
import signal
import subprocess
import time
import setproctitle
from monty.src.activity import get_status, check_activity
from monty.src.firefox import query
from monty.src.log import close_connections, insert_ranking, insert_logs

class Status(IntEnum):
    """Maps program/website rankings to a value."""

    UNKNOWN = 0
    SYSTEM = 1
    DISTRACTING = 2
    NEUTRAL = 3
    PRODUCTIVE = 4

class KillHandler():
    """Catches SIGINT and SIGTERM allowing cleanup.

    Attributes:
        killed: A boolean signifying if a signal has been caught (default=False)
    """

    killed = False
    def __init__(self):
        signal.signal(signal.SIGINT, self.exit_int)   #CTRL-C
        signal.signal(signal.SIGTERM, self.exit_term) #kill

    def exit_term(self, signum, frame):
        """Catches SIGTERM."""

        self.killed = True
        print("SIGTERM")

    def exit_int(self, signum, frame):
        """Catches SIGINT."""

        self.killed = True
        print("SIGINT")

def call_top(delay):
    """Executes top command as subprocess.

    The top bash command is being used to view what is being run locally
    on the machine.

    Args:
        delay: An int representing the number of seconds to delay top from refreshing.

    Attributes:
        top: A string composing the needed top command.
        grep: A string composing the needed grep command.
        cmd: A string that combines top and grep to be used by subprocess.

    Returns:
        A string of output generated from top of all process running owned by the user.
    """

    top = "top -bid " + str(delay) + " -u " + USER + " -n 2 -w 512"
    grep = " | grep "+ USER
    cmd = 'bash -c "exec -a Monty_Top ' + top + grep+'"'
    return subprocess.check_output(args=cmd, shell=True)

def parse_data(data, time_start, time_end):
    """Parses data generated from top and Firefox.

    Using regex, the data gathered from top command and Firefox's backups
    is organized and inserted into the sqlite database.

    Args:
        data: The string output from top.
        time_start: A unix timestamp representing a starting point.
        time_end: A unix timestamp representing an end point.

    Attributes:
        active_flag: A boolean representing if computer activity has been
            detected (mouse/keyboard movement).
        log: A set of unique process names to be added to the database.
        name: The string representing the name of a detected process.
        history: A set of unique urls to be added to the database.
        status: An int enum representing if the minute was deemed as
            HIGH/LOW/AWAY activity.
    """

    active_flag = False
    log = set()
    for line in data.decode().split('\n')[:-1]:
        #RE: PID .... #:##.## NAME
        name = re.search(r'\d+.*?\d+?:\d\d\.\d\d (.*)', line)
        if name is not None:
            name = name.group(1)
            insert_ranking(name)
            log.add(name)
            if active_flag is False and "ibus-" in name:
                active_flag = True
                print("ibus detected")
    check_activity(active_flag)
    history = query(time_start, time_end)
    for url in history:
        insert_ranking(url, 1)
        log.add(url)
    status = get_status()
    insert_logs(int(float(time_start)), ','.join(log), status.name)
    print("parse_data():", status.name)

def process_loop(exit_event, work_queue):
    """Main loop for multiprocessing.

    Args:
        exit_event: An Event used to signal any process to clean up.
        work_queue: A Queue to process data in parallel.

    """

    setproctitle.setproctitle("Monty_Subprocess")
    while not exit_event.is_set():
        if not work_queue.empty():
            parse_data(*(work_queue.get()))
        time.sleep(1)
    #finish leftovers
    print("Cleaning up queue")
    while not work_queue.empty():
        parse_data(*(work_queue.get()))

if __name__ == "__main__":
    setproctitle.setproctitle("Monty")
    print("Setup...")
    #retrieve info from .env
    USER = os.getenv("USER")
    #initialize manager, process, SIGINT handler,  etc
    DEFAULT_INT_HANDLER = signal.getsignal(signal.SIGINT) #store orginal signal handler
    DEFAULT_TERM_HANDLER = signal.getsignal(signal.SIGTERM)
    signal.signal(signal.SIGINT, signal.SIG_IGN)#ignore SIGINT for children
    signal.signal(signal.SIGTERM, signal.SIG_IGN)#ignore SIGTERM for children
    MANAGER = Manager()
    #global exit_event
    exit_event = Event()
    #global work_queue
    work_queue = Queue()
    PROC = Process(target=process_loop, args=(exit_event, work_queue))
    PROC.start()
    signal.signal(signal.SIGINT, DEFAULT_INT_HANDLER)#restore SIGINT handler
    signal.signal(signal.SIGTERM, DEFAULT_TERM_HANDLER)#restore SIGTERM handler
    HANDLER = KillHandler()
    #get data from top
    DELAY = 60 #seconds
    print("Begin program")
    while True:
        try: #try and execute cmd
            output = call_top(DELAY)
        except subprocess.CalledProcessError: #if interrupt, raise for grace exit
            pass
        else: #if no interrupt during execution, parse data
            #parse data from top
            t = time.time()
            work_queue.put((output, t-DELAY, t))
        finally: #check if interrupt was called at some point and exit
            if HANDLER.killed:
                print("Sig caught")
                exit_event.set()
                print("Joining")
                PROC.join()
                break
    close_connections()
