#!/bin/sh

if [ $# = 0 ]; then
  echo "Installing Monty"

  echo "Setting up subdirectories"
  mkdir -p db web/templates web/static ~/.config/upstart/monty

  echo "Copying Upstart files"
  cp .start.conf ~/.config/upstart/monty/start.conf
  cp .flask.conf ~/.config/upstart/monty/flask.conf
  echo "chdir `pwd`/.." | tee ~/.config/upstart/monty/start.override > ~/.config/upstart/monty/flask.override

  echo "Starting Monty"
  start monty/start

elif [ $1 = "-u" ]; then
  echo "Uninstalling Monty"

  echo "Stoping Monty"
  stop monty/start

  echo "Removing Upstart files"
  rm -rf ~/.config/upstart/monty

else
  echo "Invalid argument"
  echo "No arguments to install Monty"
  echo "\"-u\" flag to uninstall Monty"
fi
