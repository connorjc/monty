#!/usr/bin/env python
"""Checks if the screen is locked and sets activity levels."""

from enum import IntEnum
import subprocess

class Activity(IntEnum):
    """Mapping of activity levels and ints.

    This enum represents the different possible
    states of computer usage, or activity level.
    """

    AWAY = 0
    HIGH = 1
    LOW = 2

STATUS = Activity.HIGH

def get_status():
    return STATUS

def reset_activity(status):
    global STATUS
    STATUS = status

def is_screen_locked():
    #ubuntu < 16: try gnome-screensaver-command -q
    #ubuntu 16.04 using unity
    cmd = "qdbus org.gnome.ScreenSaver /com/canonical/Unity/Session \
                    com.canonical.Unity.Session.IsLocked"
    return "true" in subprocess.check_output(args=cmd, shell=True).decode()

def check_activity(HIGH):
    if is_screen_locked():
        reset_activity(Activity.AWAY)
    elif HIGH:
        reset_activity(Activity.HIGH)
    else:
        reset_activity(Activity.LOW)

if __name__ == "__main__":
    pass
