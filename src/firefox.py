#!/usr/bin/env python
"""Get firefox history if detected."""

import datetime
import os
import json
import glob
import re
import subprocess

# path to firefox browser data
PATH = os.getenv("HOME")+"/.mozilla/firefox/"
# browser data directory:
default_dir= glob.glob(PATH+"*.default") # *.default if one user
PATH = default_dir[0] + "/sessionstore-backups/recovery.jsonlz4"

def query(s,e):
    cmd = 'lz4jsoncat '+PATH
    output = subprocess.check_output(args=cmd,shell=True).decode()
    urls = set()
    for w in json.loads(output)['windows']:
        for t in w['tabs']:
            if s <= t['lastAccessed']/1000 <= e:
                data = re.search(r'https?://(.*?)[/:]',t['entries'][-1]['url'])
                if data is not None:
                    urls.add(data.group(1))
    return urls

if __name__ == "__main__":
    pass
