#!/usr/bin/env python
"""Creating/updating sqlite files."""

import sqlite3
import os

PATH = os.path.dirname(os.path.abspath(__file__))

#connect to sqlite files
conn  = sqlite3.connect(PATH+"/../db/logs.sqlite")
cur  = conn.cursor()

#create tables if they dont exist already
cur.execute(("CREATE TABLE IF NOT EXISTS Logs ("
             "timestamp datetime PRIMARY KEY, "
             "apps text, "
             "activity text);"))
cur.execute(("CREATE TABLE IF NOT EXISTS Ranking ("
             "name text PRIMARY KEY, "
             "rank integer, "
             "website integer);"))

def insert_logs(timestamp, apps, activity):
    cur.execute(("INSERT INTO Logs "
                 "VALUES (DATETIME(?, 'unixepoch', 'localtime'),?,?)"), \
                 (timestamp, apps, activity))
    conn.commit()

def insert_ranking(name, website=0):
    """Inserts into table if entry is not found, otherwise do nothing."""

    cur.execute('INSERT or IGNORE INTO Ranking VALUES (?,?,?)', \
            (name, 0, website))
    conn.commit()

def close_connections():
    conn.close()

if __name__ == "__main__":
    close_connections()
