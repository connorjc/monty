"""Main driver for frontend of the monty web application.

This file is the main logic for the frontend of the application. It is run
independently from the backend's scripts. As a flask server, this script
gets data from the shared sqlite database and parses it such that the required
data is in a suitable format to create informative and intuitive graphs in the
browser.

As this is a non-invasive program, and the data gathered may be deemed
personal, all the data is saved in the sqlite database that you can remove
at anytime. No duplicates are ever created.

Author: Connor Christian
"""

import os
import datetime
import time
from calendar import monthrange
import setproctitle
from flask import Flask, render_template, request, redirect, url_for, jsonify, abort
from flask_api import FlaskAPI

def create_app(test_config=None):
    """Excuted by flask to render the webpage."""

    setproctitle.setproctitle("Monty_Flask")
    # create and configure the app
    app = FlaskAPI(__name__, instance_relative_config=True)
    app.config.from_mapping(DATABASE=os.path.join(os.path.dirname(os.path.abspath(__file__)),\
            "../db/logs.sqlite"))

    from . import db
    db.init_app(app)

    @app.route("/")
    def index():
        """Homepage redirects to view today's stats."""
        return redirect(url_for('day'))

    @app.route("/day")
    @app.route("/day/<date>")
    @app.route("/day/<date>/<int:time>")
    def day(date=False, time=-2):
        """Gathers data for a single day."""

        if not date:
            date = str(datetime.date.today())
            hour = str(datetime.datetime.now().time().hour)
            return redirect(url_for('day')+'/'+date+'/'+hour)
        else:
            data = getData(date, time)
            min_AWAY, min_LOW, min_HIGH, activity = data[4], data[5], data[6], data[7]
            prod_gauge, web_prog, program_data, website_data, unclassified = classifyData(data, time)
        return render_template('day.html', minutes=(min_HIGH, min_LOW, min_AWAY), date=str(date), time=time, \
                activity=activity, hourlyData=(prod_gauge, web_prog, program_data, website_data, unclassified))

    @app.route("/week")
    @app.route("/week/<int:date>")
    @app.route("/week/<int:year>/<int:date>")
    def week(year=datetime.date.today().isocalendar()[0], date=False):
        """Gathers data for a single week."""

        activity = []
        weekNames = ['Sun','Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat']
        labels = []
        totalActivity = [[], [], [], [], [], []] # Away Low High Distracted Productive Neutral
        top_prog = dict()
        top_web = dict()
        if not date:
            delta = datetime.timedelta(days=1)
            date = datetime.date.today()
            for i in range(7):
                day = date.timetuple()
                names = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'][day.tm_wday]
                day = "{}-{}-{}".format(year, day.tm_mon, day.tm_mday)
                dayStats(day, names, totalActivity, activity, top_prog, top_web)
                date -= delta
                labels.append(day[5:])
            activity.reverse()
            labels.reverse()
            totalActivity[0].reverse()
            totalActivity[1].reverse()
            totalActivity[2].reverse()
            totalActivity[3].reverse()
            totalActivity[4].reverse()
            totalActivity[5].reverse()
            date = datetime.date.today().isocalendar()[1]
        else:
            for i,names in enumerate(weekNames):
                day = time.strptime('{} {} {}'.format(year, date-1, i), '%Y %U %w')
                day = "{}-{}-{}".format(year, day.tm_mon, day.tm_mday)
                dayStats(day, names, totalActivity, activity, top_prog, top_web)
                labels.append(day[5:])
        top_prog = sorted(top_prog.items(), key=lambda x:x[1], reverse=True)[:5]
        top_web = sorted(top_web.items(), key=lambda x:x[1], reverse=True)[:5]
        top_names = [[],[]]
        top_data  = [[],[]]
        for i in range(5):
            #programs
            if i <= len(top_prog)-1:
                top_names[0].append(top_prog[i][0])
                top_data[0].append(top_prog[i][1])
            #web
            if i <= len(top_web)-1:
                top_names[1].append(top_web[i][0])
                top_data[1].append(top_web[i][1])
        return render_template('week.html', week=date, year=year, activity=activity, barGraph=totalActivity, labels=labels, top_names=top_names, top_data=top_data)

    @app.route("/month")
    @app.route("/month/<int:date>")
    @app.route("/month/<int:year>/<int:date>")
    def month(year=datetime.date.today().isocalendar()[0], date=False):
        """Gathers data for a single month."""

        month = ''
        activity = []
        labels = []
        label = ''
        if not date:
            date = datetime.date.today()
            delta = datetime.timedelta(days=1)
            month = datetime.datetime.strptime('{} {}'.format(year, date.timetuple().tm_mon), '%Y %m')
            for i in range(30):
                day = date.timetuple()
                day = "{}-{}-{}".format(year, day.tm_mon, day.tm_mday)
                activity.append(dayStats(day))
                date -= delta
                labels.append(day[5:])
            activity.reverse()
            labels.reverse()
            label = "Past 30 days"
        else:
            month = datetime.datetime.strptime('{} {}'.format(year, date), '%Y %m')
            day_of_year = month.timetuple().tm_yday
            for i in range(monthrange(year, date)[1]):
                day = time.strptime('{} {}'.format(year, day_of_year+i), '%Y %j')
                day = "{}-{}-{}".format(year, day.tm_mon, day.tm_mday)
                activity.append(dayStats(day))
                labels.append(str(i+1))
            label = time.strftime('%b %Y', month.timetuple())
        month = time.strftime('%B %Y', month.timetuple())
        return render_template('month.html', month=month, label='"{}"'.format(label), lineChart=activity, labels=labels)

    def dayStats(day, names=None, totalActivity=None, activity=None, top_prog=None, top_web=None):
        """Organizes data from a single day."""

        a = l = h = d = p = n = total_act = total_prod = 0
        for t in range(24):
            data = getData(day, t)
            classified = classifyData(data, t)
            prod, prog_data, web_data = classified[0], classified[2], classified[3]
            if top_prog is not None:
                for name, _, freq, __ in prog_data:
                    v = top_prog.get(name,0)
                    top_prog[name] = v+freq
            if top_web is not None:
                for name, _, freq, __ in web_data:
                    v = top_web.get(name,0)
                    top_web[name] = v+freq
            a += data[4]
            l += data[5]
            h += data[6]
            d += prod[0]
            p += prod[1]
            n += prod[2]
        total_act = (a+l+h)/60
        total_prod = (d+p+n)/60
        a /= 60
        l /= 60
        h /= 60
        d /= 60
        p /= 60
        n /= 60
        if totalActivity is not None:
            totalActivity[0].append(a)
            totalActivity[1].append(l)
            totalActivity[2].append(h)
            if total_prod != 0:
                totalActivity[3].append((d/total_prod)*total_act)
                totalActivity[4].append((p/total_prod)*total_act)
                totalActivity[5].append((n/total_prod)*total_act)
            else:
                totalActivity[3].append(0)
                totalActivity[4].append(0)
                totalActivity[5].append(0)
        if activity is not None:
            activity.append((names, day, colorWeek(data[7])))

        if total_prod != 0:
            return (p/total_prod)*total_act - (d/total_prod)*total_act
        else:
            return 0

    @app.route("/update", methods=["GET", "POST"])
    @app.route("/update/<classification>", methods=["GET", "POST"])
    def crud(classification="All"):
        """Read/Update/Delete access to database via dynamic table."""

        if request.method == "POST":
            for program in request.form:
                updateRank(program, str(request.form[program]))

        classification = classification.capitalize()
        if classification == "All":
            db_data = getRanks()
            return render_template('crud.html', type="All", data=db_data)
        else:
            labels = ["Unknown", "System", "Distracting", "Neutral", "Productive"]
            index = labels.index(classification)
            db_data = getRanks(index)
            return render_template('crud.html', type=labels[index], data=db_data)

    def colorWeek(classified_hours):
        """Determines level activity of a day."""

        label = None
        data = [h[1] for h in classified_hours if h[1] is not None]
        if data:
            label = max(data, key=data.count)
        return label

    def getData(date, time):
        """Organizes data to be used by day template."""

        sqlite = db.get_db()
        date = datetime.datetime.strptime(date, "%Y-%m-%d").date()
        next_date = date + datetime.timedelta(days=1)
        activity = sqlite.execute((
            "SELECT * "
            "FROM Logs WHERE timestamp "
            "BETWEEN ? AND ?"), (str(date), str(next_date))).fetchall()
        data_map = dict()
        distracted_map = dict()
        distracted_hours = set()
        all_programs = []
        away_apps = []
        low_apps = []
        high_apps = []
        for t, apps, a in activity:
            apps = apps.split(',')
            h = datetime.datetime.strptime(t,"%Y-%m-%d %H:%M:%S").time().hour
            val = data_map.get(h, [])
            val.append(a)
            data_map[h] = val
            productivity = sqlite.execute((
                "SELECT rank "
                "FROM Ranking "
                "WHERE name IN ({}) ".format(', '.join('?' for _ in apps))), apps).fetchall()
            val = distracted_map.get(h, [])
            val.extend([p[0] for p in productivity if p[0] == 2 or p[0] == 4])
            distracted_map[h] = val
            if h == time:
                if a == "AWAY":
                    away_apps.extend(apps)
                if a == "LOW":
                    low_apps.extend(apps)
                if a == "HIGH":
                    high_apps.extend(apps)
                all_programs.extend(apps)
        away_apps = set(away_apps)
        low_apps = set(low_apps)
        high_apps = set(high_apps)
        for h,r in distracted_map.items():
            d = r.count(2)
            p = r.count(4)
            if d >= p:
                distracted_hours.add(h)
        min_AWAY = min_LOW = min_HIGH = 0
        for k,v in data_map.items():
            a = v.count("AWAY")
            l = v.count("LOW")
            h = v.count("HIGH")

            if a > l and a > h:
                data_map[k] = "AWAY"
            elif k in distracted_hours:
                data_map[k] = "DISTRACTED"
            elif h > l and h > a:
                data_map[k] = "HIGH"
            else: #if l >= h and l > a:
                data_map[k] = "LOW"

            if k == time:
                min_HIGH += h
                min_LOW += l
                min_AWAY += a
        for k in range(24):
            data_map.setdefault(k)
        activity = [(k, data_map[k]) for k in range(24)]
        return all_programs, away_apps, low_apps, high_apps, min_AWAY, min_LOW, min_HIGH, activity

    def classifyData(data, time):
        """Organizes data for graphs in day template."""

        sqlite = db.get_db()
        all_programs, away_apps, low_apps, high_apps = data[0], data[1], data[2], data[3]
        web_prog = [0, 0]
        prod_gauge = [0, 0, 0]
        programs = ()
        program_labels = []
        program_ranks = []
        program_classes = []
        web_freq = []
        web_labels = []
        web_ranks = []
        web_classes = []
        program_freq = []
        unclassified_labels = []
        unclassified_freq = []
        unclassified_classes = []
        if time >= -1:
            programs = sorted(list(set(all_programs)))
            hourly_data = sqlite.execute((
                "SELECT * "
                "FROM Ranking "
                "WHERE name IN ({}) "
                "AND rank != 1".format(', '.join('?' for _ in programs))), programs).fetchall()
            for n, r, w in hourly_data:
                classes = ""
                if r == 0: #unclassified
                    unclassified_labels.append(n)
                    unclassified_freq.append(all_programs.count(n))
                    if n in away_apps:
                        classes += "Away "
                    if n in low_apps:
                        classes += "Low "
                    if n in high_apps:
                        classes += "High "
                    unclassified_classes.append(classes.rstrip())
                elif r != 1: #skip system programs
                    freq = 0
                    if bool(w): #websites
                        web_prog[0] += 1
                        web_labels.append(n)
                        web_ranks.append(r)
                        freq = all_programs.count(n)
                        web_freq.append(freq)
                        if n in away_apps:
                            classes += "Away "
                        if n in low_apps:
                            classes += "Low "
                        if n in high_apps:
                            classes += "High "
                        web_classes.append(classes.rstrip())
                    else: #programs
                        web_prog[1] += 1
                        program_labels.append(n)
                        program_ranks.append(r)
                        freq = all_programs.count(n)
                        program_freq.append(freq)
                        if n in away_apps:
                            classes += "Away "
                        if n in low_apps:
                            classes += "Low "
                        if n in high_apps:
                            classes += "High "
                        program_classes.append(classes.rstrip())
                    if r == 3:#neutral
                        prod_gauge[2] += freq
                    elif r == 4:#productive
                        prod_gauge[1] += freq
                    elif r == 2:#distracting
                        prod_gauge[0] += freq
        program_data = list(zip(program_labels, program_ranks, program_freq, program_classes))
        website_data = list(zip(web_labels, web_ranks, web_freq, web_classes))
        unclassified = list(zip(unclassified_labels, unclassified_freq, unclassified_classes))
        return prod_gauge, web_prog, program_data, website_data, unclassified

    def getRanks(index=None):
        """Helper function for crud(), returns data from database."""

        sqlite = db.get_db()
        if index is not None:
            return sqlite.execute((
                "SELECT * "
                "FROM Ranking "
                "WHERE rank = ?"), (index,)).fetchall()
        else:
            return sqlite.execute(
                "SELECT * "
                "FROM Ranking ").fetchall()

    def updateRank(name, rank):
        """Helper function for crud(), updates database."""

        sqlite = db.get_db()
        if int(rank) >= 0:
            sqlite.execute((
                "UPDATE RANKING "
                "SET rank = ? "
                "WHERE name = ?"), (rank, name))
        else:
            sqlite.execute((
                "DELETE FROM RANKING "
                "WHERE name = ?"), (name,))
        sqlite.commit()

    @app.route("/export")
    @app.route("/export/<app>")
    def export(app=None):
        """API access to database that returns data as JSON."""

        sqlite = db.get_db()
        if app is None:
            data = sqlite.execute("SELECT * FROM Ranking").fetchall()
            results = {'rank':{0:[], 1:[], 2:[], 3:[], 4:[]}, 'log':[]}
            for name, rank, web in data:
                results['rank'][rank].append({
                    'id': name,
                    'web': bool(web)
                })
            data = sqlite.execute("SELECT * FROM Logs ORDER BY date(timestamp)").fetchall()
            for stamp, apps, activity in data:
                results['log'].append({
                    'timestamp': stamp,
                    'apps': apps.split(','),
                    'activity': activity
                })
            response = jsonify(results)
            response.status_code = 200
            return response
        else:
            data = sqlite.execute("SELECT * FROM Ranking WHERE name LIKE ?", ('%{}%'.format(app),)).fetchall()
            if data == []:
                abort(404)
            results = []
            for name, rank, web in data:
                results.append({
                    'id': name,
                    'rank': rank,
                    'web': bool(web)
                })
            response = jsonify(results)
            response.status_code = 200
            return response

    return app
