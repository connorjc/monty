var app = angular.module('myApp', []);

app.controller('myCtrl', ['$scope', function($scope) {
    $scope.date;

    $scope.view = "day";
    $scope.pickDay = function() {
        $scope.view = "day";
    };
    $scope.pickWeek = function() {
        $scope.view = "week";
    };
    $scope.pickMonth = function() {
        $scope.view = "month";
    };
    $scope.pickYear = function() {
        $scope.view = "year";
    };

    $scope.hour;
}]);

app.config(function($interpolateProvider){
    $interpolateProvider.startSymbol('[[').endSymbol(']]');
});

$(document).ready(function () {
  $('#crudTable').DataTable();
  $('.dataTables_length').addClass('bs-select');
});
